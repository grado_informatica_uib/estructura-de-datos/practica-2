# PRACTICA 2: TABLERO TRES EN RAYA.

La práctica consiste en generar y mostrar el espacio de estados para el juego tres en raya.

## Autores.

Los autores de la práctica son los siguientes alumnos de la asignatura **Estructura de Datos** del **Grado en Ingeniería Informática**  impartido en la **UIB**:
- Héctor Mario Medina Cabanelas @hector.medina / hmedcab@gmail.com
- Juan Carlos Boo Crugeiras

## Programa principal.

El programa principal ha de encargarse de:

1. Solicitar al usuario que introduzca por teclado el estado del juego para el cual se quiere general el espacio de estados.
2. Generar el espacio de estados utilizando el TAD árbol ordinario visto en la clase de prácticas.
3. Mostrar el espacio de estados generados utilizando el recorrido en amplitud del árbol ordinario visto en clase de prácticas.

## Estado del juego.

El estado del juego consiste en el tablero con los movimientos realizados juntamente con la información que ha realizado el último movimiento.

## Obtenición del estado inicial.

Para obtener el estado inicial para general el espacio de estados, el usuario ha de introducit por teclado un *String* de longitud 9 con la información de las piezas que contienen cada una de las 9 casillas del tablero.
La primera posición del *String* ha de contener la pieza que ocupa la casilla (1,1) del tablero, la segunda posición del *String* ha de contener la pieza que ocupa la casilla (1,2) del tablero, la tercera posición del *String* ha de contener la pieza que ocupa la casilla (1,3) del tablero, la cuarta posición del *String* ha de contener la pieza que ocupa la casilla (2,1) del tablero, y así sucesivamente.
Por ejemplo, el *String*  *O - X X - -  X O O* habría de generar el tablero:

<img style="display:block; margin: 0 auto" src="images/Tablero inicial.JPG">


La pieza *X* se corresponde con la pieza del jugador 1, la pieza *O* se corresponde con la pieza del jugador 2 y la pieza *-* se corresponde con una casilla desocupada del tablero.
Además de la información de las piezas ocupadas por cada uno de los jugadores en el tablero, el usuario también ha de introducit qué jugador ha realizado el último movimiento (jugador 1 o jugador 2).

## Espacio de estados.

Un espacio de estados de un juego consiste en el árbol de todos los posibles estados que pueden generarse a partir de aplicar todos los movimientos posibles en un estado concreto.
Por ejemplo, el estado de estacios general a partir del estado de ejemplo de la sección *Estado del juego* sería:

<img style="display:block; margin: 0 auto" src="images/Espacio de estados.JPG">


## Se solicita.

1. Implementar una cola (utilizando cursores o punteros).

    1.1. Ha de ser genérica: ha de poder contener ítems de cualquier tipo de dato.

    1.2. Ha de ser TAD.

2. Implementar un árbol ordinario (según la implementación vista en clase de prácticas)

    2.1. Ha de ser genérico: ha de poder contener ítems de cualquier tipo de dato.

    2.2. Ha de ser TAD.

3. Realizar un **programa principal que solicite al usuario el estado inicial del juego** (estado de las piezas del tablero y último jugador que ha realizado el movimiento) y **genere el espacio de estados correspondiente a este estado inicial** (Un espacio de estados se corresponde con el árbol de todos los posibles estados que pueden generarse a partir del estado inicial y los movimientos posibles). Finalmente el estado de estados generado ha de mostrarse mediante un **recorrido en amplitud sobre el árbol de estados generado**.