-------------------------------------------------------------------------------
--                              DARBOLORDINARIO.ADB
-- DESCRIPCION:
-- Este fichero se corresponde con la implementacion del tipo de dato 'arbol
-- ordinario'.
-- AUTORES: Hector Mario Medina Cabanelas - hector.medina1@estudiant.uib.es
--          Juan Carlos Boo Crugeiras -
-- REPOSITORIO GITLAB:
-- https://gitlab.com/grado_informatica_uib/estructura-de-datos/practica-2
-------------------------------------------------------------------------------
-- Implementacion del package dcola.
package body darbolordinario is
   ----------------------------------------------------------------------------
   -- AVACIO(): Vacia el arbol.
   ----------------------------------------------------------------------------
   procedure avacio(t: out arbol) is
      raiz : pnode renames t.raiz;
   begin
      -- Simplemente hacemos que el arbol no tenga ningun nodo.
      raiz := null;
   end avacio;
   ----------------------------------------------------------------------------
   -- ESTA_VACIO(): Comprueba si el arbol esta vacia y devuelve TRUE o FALSE.
   ----------------------------------------------------------------------------
   function esta_vacio(t: in arbol) return boolean is
      raiz : pnode renames t.raiz;
   begin
      if raiz = null then
         return TRUE;
      else
         return FALSE;
      end if;
   end esta_vacio;
   ----------------------------------------------------------------------------
   -- ANADIR_HIJO(): A�ade un hijo a la raiz del arbol.
   ----------------------------------------------------------------------------
   procedure anadir_hijo(t: in out arbol; x: in elem) is
      raiz : pnode renames t.raiz;
      p : pnode;
   begin
      -- Si la raiz en nula, no hay nodo raiz y no se puede a�adir hijo.
      if raiz = null then
         raise mal_uso;
      -- Si la raiz no es nula, podemos a�adir el hijo.
      else
         -- Si no tiene hijo
         if raiz.primer_hijo = null then
            -- Creamos nuevo nodo
            p := new node'(x, raiz, null,null);
            -- Asignamos a ese nodo como primer hijo de la raiz del arbol.
            raiz.primer_hijo := p;
         -- Si ya tiene un hijo
         else
            -- Cogemos el nodo 'primer hijo'
            p := raiz.primer_hijo;
            -- Mientras tenga hermano, seguimos recorriendo los hermanos.
            while p.hermano /= null loop
               -- Actualizamos al siguiente hermano.
               p := p.hermano;
            end loop;
            -- Anadimos un nuevo nodo hermano
            p.hermano := new node'(x, raiz, null, null);
         end if;
      end if;
   exception
      when Storage_Error => raise espacio_desbordado;
   end anadir_hijo;
   ----------------------------------------------------------------------------
   -- E_PRIMER_HIJO(): Comprueba si la raiz de un arbol tiene hijos.
   ----------------------------------------------------------------------------
   function e_primer_hijo(t: in arbol) return boolean is
      raiz : pnode renames t.raiz;
   begin
      -- Si el puntero no esta vacio
      if raiz.primer_hijo /= null then
         -- Tiene hijo.
         return True;
      -- Si es null
      else
         -- No tiene hijo.
         return False;
      end if;
   exception
         when Constraint_Error => raise mal_uso;
   end e_primer_hijo;
   ----------------------------------------------------------------------------
   -- PRIMER_HIJO(): Devuelve el subarbol formado por el hijo de la raiz de t.
   ----------------------------------------------------------------------------
   procedure primer_hijo(t: in arbol; ct: out arbol) is
      rt : pnode renames t.raiz;
      rct : pnode renames ct.raiz;
   begin
      if rt.primer_hijo = null then
         raise mal_uso;
      end if;
      rct := rt.primer_hijo;
   end primer_hijo;
   ----------------------------------------------------------------------------
   -- ATOM(): Crea la raiz de un arbol.
   ----------------------------------------------------------------------------
   procedure atom(t: out arbol; x: in elem) is
      raiz : pnode renames t.raiz;
   begin
      -- Si la raiz no es nula
      if raiz /= null then
         -- Se ha usado mal.
         raise mal_uso;
      end if;
      -- Creamos un nodo nuevo y lo asociamos a la raiz.
      raiz := new node'(x, null, null, null);
   end atom;
   ----------------------------------------------------------------------------
   -- RAIZ(): Devuelve el elemento raiz de un arbol.
   ----------------------------------------------------------------------------
   function raiz(t: in arbol) return elem is
      raiz : pnode renames t.raiz;
   begin
      -- Devolvemos el elemento de la raiz.
      return raiz.c;
   exception
      when Constraint_Error => raise mal_uso;
   end raiz;
   ----------------------------------------------------------------------------
   -- E_HERMANO(): Indica si la raiz t tiene un hermano.
   ----------------------------------------------------------------------------
   function e_hermano(t: in arbol) return boolean is
      raiz : pnode renames t.raiz;
   begin
      -- Si tiene hermano.
      if raiz.hermano /= null then
         -- Devolvemos true
         return True;
      -- Si no tiene hermano.
      else
         -- Devolvemos false.
         return False;
      end if;
   -- En caso de errores.
   exception
      when Constraint_Error => raise mal_uso;
   end e_hermano;
   ----------------------------------------------------------------------------
   -- HERMANO(): Devuelve el subarbol formado por el hermano de la raiz de t.
   ----------------------------------------------------------------------------
   procedure hermano(t: in arbol; st: out arbol) is
      rt : pnode renames t.raiz;
      rst : pnode renames st.raiz;
   begin
      -- Asignamos el arbol hermano del arbol t.
      rst := rt.hermano;
   exception
         when Constraint_Error => raise mal_uso;
   end hermano;
   ----------------------------------------------------------------------------
   -- E_PADRE(): Indica si t tiene padre devolviendo TRUE o FALSE.
   ----------------------------------------------------------------------------
   function e_padre(t: in arbol) return boolean is
      raiz : pnode renames t.raiz;
   begin
      -- Si no tiene padre.
      if raiz.padre = null then
         -- Devolvemos falso.
         return False;
      -- Si tiene padre
      else
         -- Devolvemos verdadero.
         return True;
      end if;
   exception
      when Constraint_Error => raise mal_uso;
   end e_padre;
   ----------------------------------------------------------------------------
   -- PADRE(): Devuelve el arbol formado por el padre de la raiz de t.
   ----------------------------------------------------------------------------
   procedure padre(t: in arbol; pt: out arbol) is
      rt : pnode renames t.raiz;
      rpt : pnode renames pt.raiz;
   begin
      -- Asignamos el puntero al padre a la raiz del arbol padre.
      rpt := rt.padre;
   exception
      when Constraint_Error => raise mal_uso;
   end padre;
   ----------------------------------------------------------------------------
   -- AMPLITUD(): Recorrido en amplitud del arbol.
   ----------------------------------------------------------------------------
   procedure amplitud(t: in arbol) is
      package acola is new dcola(pnode);
      use acola;
      c: acola.cola;
      pt: pnode renames t.raiz;
      p: pnode;
   begin
      if pt = null then
         raise mal_uso;
      end if;
      p:= pt;
      cvacia(c);
      poner(c,p);

      while not esta_vacia(c) loop
         p := coger_primero(c);
         borrar_primero(c);
         visit(p.c);
         p:= p.primer_hijo;
         while p /= null loop
            poner(c,p);
            p := p.hermano;
         end loop;
      end loop;
   end amplitud;
   ----------------------------------------------------------------------------
   -- AMPLITUD(): Recorrido en amplitud del arbol.
   ----------------------------------------------------------------------------
   procedure amplitud(t: in arbol; q: out dcolaelem.cola) is
      package acola is new dcola(pnode);
      use acola;
      c: acola.cola;
      pt: pnode renames t.raiz;
      p: pnode;
   begin
      if pt = null then
         raise mal_uso;
      end if;
      p:= pt;
      cvacia(c);
      poner(c,p);

      while not esta_vacia(c) loop
         p := coger_primero(c);
         borrar_primero(c);
         poner(q, p.c);
         p:= p.primer_hijo;
         while p /= null loop
            poner(c,p);
            p := p.hermano;
         end loop;
      end loop;
   end amplitud;

end darbolordinario;
