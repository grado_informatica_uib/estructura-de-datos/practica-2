with dtauler;
use  dtauler;
with darbolordinario;
with dcola;
with Ada.Text_IO;
use  Ada.Text_IO;
with Ada.Integer_Text_IO;
use  Ada.Integer_Text_IO;

procedure Main is

   type estat is record
      t: tauler;
      jugador: integer;
   end record;

   procedure print (s: in estat) is
   begin
      Put_Line("Jugador: " & s.jugador'Image);
      print(s.t);
   end print;
      --Declaraci�n de una cola de estados
   package instacola is new dcola(estat);
   use instacola;

   package darboltauler is new darbolordinario(estat, print,instacola);
   use darboltauler;

   type parbol is access arbol;
   package dcolaarbol is new dcola(parbol);
   use dcolaarbol;

   estado : estat;              -- Estado inicial
   t : tauler;                  -- Tablero
   c : tcella;                  -- Celda
   line : String(1..9);         -- String introducido por usuario
   player : Integer;            -- Almacenamos id jugador
   jugador : Integer;           -- Variable auxiliar para almacenar jugador
   ii : Integer;                -- Controlador de indices del String
   arbolito : arbol;            -- Arbol que generaremos.
   ----------------------------------------------------------------------------
   -- GENERARARBOL(): Genera el arbol de estados.
   ----------------------------------------------------------------------------
   procedure generarArbol (tree: in out arbol) is
      estadoaux : estat;    -- Estado
      arbolhijo : arbol;    -- Arbol hijo
   begin
      -- Recorrido de celdas (filas) del tablero.
      for fila in 1..3 loop
         -- Recorrido de celdas (tablero) del tablero
         for columna in 1..3 loop
            c.fila := fila;
            c.columna := columna;
            -- Si la casilla esta vacia
            if isCasellaBuida(raiz(tree).t, c) then
               -- Cogemos el estado actual
               estadoaux := raiz(tree);
               -- Cogemos el jugador, actualizandolo.
               estadoaux.jugador := (raiz(tree).jugador mod 2)+1;
               -- Movemos en el tablero al jugador en la celda.
               mouJugador(estadoaux.t,estadoaux.jugador,c);
               -- Si no tiene hijo.
               if not e_primer_hijo(tree) then
                  -- A�adimos el hijo
                  anadir_hijo(tree, estadoaux);
                  -- Almacenamos en arbolhijo al arbol del hijo.
                  primer_hijo(tree, arbolhijo);
               -- Si ya tiene un hijo.
               else
                  -- A�adimos el hijo
                  anadir_hijo(tree, estadoaux);
                  -- Cogemos el hermano del arbol hijo.
                  hermano(arbolhijo,arbolhijo);
               end if;
               -- Si el tablero no est� lleno ni se ha ganado el juego
               if  not (isTaulerComplet(raiz(arbolhijo).t) or isJocGuanyat(raiz(arbolhijo).t, estadoaux.jugador))then
                  -- Llamamos recursivamente al algoritmo con el arbol hijo.
                  generarArbol(arbolhijo);
              end if;
            end if;
         end loop;
      end loop;
   end generarArbol;
begin
   ii := 1;
   Put_Line("****************************************************************");
   Put_Line("*                    PROGRAMA PRACTICA 2                       *");
   Put_Line("****************************************************************");
   Put_Line("");
   Put_Line("1. Introduce estado inicial 'Tablero'[9 caracteres - O X]: ");
   Get(line);
   Put_Line("Has introducido: "&line);
   Put_Line("");
   Put_Line("2. Introduce jugador que ha hecho el ultimo movimiento: ");
   Get(player);
   Put_Line("El ultimo jugador fue: "&player'Image);

   -- CREACION DEL ESTADO INICIAL
   for i in 1..3 loop
      c.fila := i;
      for j in 1..3 loop
         c.columna :=j;
         jugador := getJugador(line(ii)&"");
         mouJugador(t,jugador,c);
         ii := ii + 1;
      end loop;
   end loop;

   -- ESTADO INICIAL
   estado.t := t;
   estado.jugador := player;

   -- A�ADIMOS EL ESTADO INICIAL COMO RAIZ DEL ARBOL.
   atom(arbolito,estado);

   -- GENERAMOS ARBOL
   generarArbol(arbolito);

   -- RECORRIDO DE AMPLITUR.
   amplitud(arbolito);
end Main;
