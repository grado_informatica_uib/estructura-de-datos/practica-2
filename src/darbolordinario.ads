-------------------------------------------------------------------------------
--                         DARBOLORDINARIO.ADS
-- DESCRIPCION: 
-- Este fichero se corresponde con la parte publica de la definicion del arbol 
-- ordinario.
-- AUTORES: Hector Mario Medina Cabanelas - hector.medina1@estudiant.uib.es
--          Juan Carlos Boo Crugeiras -  
-- REPOSITORIO GITLAB:
-- https://gitlab.com/grado_informatica_uib/estructura-de-datos/practica-2
-------------------------------------------------------------------------------
-- Usaremos la cola. 
with dcola;

-- Definimos la clase como generica.
generic
   type elem is private;
   
   with procedure visit(x: in elem);
   
   -- Instanciamos la cola con el elemento correspondiente. 
   with package dcolaelem is new dcola(elem);
   -- Hacemos accesibles los atributos y metodos directamente.
   use dcolaelem;
-- Package arbol ordinario.
package darbolordinario is
   ----------------------------------------------------------------------------
   --                         ATRIBUTOS DE LA E.D.
   ----------------------------------------------------------------------------
   type arbol is limited private;     -- El arbol.
   mal_uso: exception;                -- Excepcion de mal uso.
   espacio_desbordado: exception;     -- Excepcion de espacio desbordado

   ----------------------------------------------------------------------------
   --                         METODOS DE LA E.D.
   ----------------------------------------------------------------------------
   -- Vacia el arbol.
   procedure avacio(t: out arbol);
   -- Comprueba si el arbol esta vacia y devuelve TRUE o FALSE.
   function esta_vacio(t: in arbol) return boolean;
   -- Añade un hijo a la raiz del arbol.
   procedure anadir_hijo(t: in out arbol; x: in elem);
   -- Comprueba si la raiz de un arbol tiene hijos.
   function e_primer_hijo(t: in arbol) return boolean;
   -- Devuelve el subarbol formado por el hijo de la raiz de t.
   procedure primer_hijo(t: in arbol; ct: out arbol);
   -- Crea la raiz de un arbol.
   procedure atom(t: out arbol; x: in elem);
   -- Devuelve el elemento raiz de un arbol.
   function raiz(t: in arbol) return elem;
   -- Indica si la raiz t tiene un hermano. 
   function e_hermano(t: in arbol) return boolean;
   -- Devuelve el subarbol formado por el hermano de la raiz de t.
   procedure hermano(t: in arbol; st: out arbol);
   -- Indica si t tiene padre devolviendo TRUE o FALSE
   function e_padre(t: in arbol) return boolean;
   -- Devuelve el arbol formado por el padre de la raiz de t
   procedure padre(t: in arbol; pt: out arbol);
   -- Reccordido en amplitud del arbol.
   procedure amplitud(t: in arbol);
   -- Recorrido en amplitud del arbol.
   procedure amplitud(t: in arbol; q: out dcolaelem.cola);

-- Implementacion privada. 
private
   -- Declaracion tipo nodo
   type node;
   -- Puntero a un nodo.
   type pnode is access node;
   -- Implementacion del nodo.
   type node is record
      c: elem;           -- elemento
      padre: pnode;      -- Puntero al padre.
      hermano: pnode;    -- Puntero al hermano.
      primer_hijo: pnode;-- Puntero al primer hijo.
   end record;

   -- Implementacion del arbol.
   type arbol is record
      raiz: pnode;    -- Puntero a la raiz. 
   end record;


end darbolordinario;
