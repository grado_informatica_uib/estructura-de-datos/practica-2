-------------------------------------------------------------------------------
--                                  DCOLA.ADS
-- DESCRIPCIÓN: 
-- Este fichero se corresponde con la declaración del tipo de dato 'cola'.
-- AUTORES: Héctor Mario Medina Cabanelas - hector.medina1@estudiant.uib.es
--          Juan Carlos Boo Crugeiras -  
-- REPOSITORIO GITLAB:
-- https://gitlab.com/grado_informatica_uib/estructura-de-datos/practica-2
-------------------------------------------------------------------------------
-- Declaración geneérica del tipo de dato elem.
generic
   type elem is private;
-- Paquete dcola
package dcola is
   ----------------------------------------------------------------------------
   --                         ATRIBUTOS DE LA E.D.
   ----------------------------------------------------------------------------
   type cola is limited private;        -- La cola.
   mal_uso: exception;                  -- Excepcion de mal uso.
   espacio_desbordado: exception;       -- Excepcion de espacio desbordado.

   ----------------------------------------------------------------------------
   --                          MÉTODOS DE LA E.D.
   ----------------------------------------------------------------------------
   -- Vacia la cola.
   procedure cvacia(qu: out cola);  
   -- Pone un elemento en la cola.
   procedure poner(qu: in out cola; x: in elem);
   -- Borra el primer elemento de la cola.
   procedure borrar_primero(qu: in out cola);
   -- Devuelve el elemento primero de la cola.
   function coger_primero(qu: in cola) return elem;
   -- Comprueba si la cola esta o no vaci�a y devuelve un booleano.
   function esta_vacia(qu: in cola) return boolean;
   
-- Implementacion de la cola: Lo haremos utilizando punteros.
private
   -- Declaracion del tipo nodo.
   type nodo;
   -- Declaracion del puntero al nodo.
   type pnodo is access nodo;
   -- Especificacion del tipo nodo.
   type nodo is record
      x: elem;        -- Elemento.
      sig: pnodo;     -- Puntero.
   end record;
   -- Declaracion de la cola.
   type cola is record
      -- La cola simplemente son dos punteros. 
      p, q: pnodo;
   end record;
end dcola;
