-------------------------------------------------------------------------------
--                                  DCOLA.ADB
-- DESCRIPCION: 
-- Este fichero se corresponde con la implementacion del tipo de dato 'cola'.
-- AUTORES: Hector Mario Medina Cabanelas - hector.medina1@estudiant.uib.es
--          Juan Carlos Boo Crugeiras -  
-- REPOSITORIO GITLAB:
-- https://gitlab.com/grado_informatica_uib/estructura-de-datos/practica-2
-------------------------------------------------------------------------------
-- Implementacion del package dcola.
package body dcola is
   ----------------------------------------------------------------------------
   -- CVACIA(): Vaci�a la cola.
   ----------------------------------------------------------------------------
   procedure cvacia(qu: out cola) is  
      p: pnodo renames qu.p;
      q: pnodo renames qu.q;
   begin
      p:=null;
      q:=null;
   end cvacia;
   
   ----------------------------------------------------------------------------
   -- PONER(): Pone un elemento en la cola.
   ----------------------------------------------------------------------------
   procedure poner(qu: in out cola; x: in elem) is
      p: pnodo renames qu.p;
      q: pnodo renames qu.q;
      r: pnodo;
   begin   
      r:= new nodo'(x,null);
      if p=null then
         p:=r;
         q:=r;
      else
         p.sig:=r;
         p:=r;
      end if;
   exception
         when Storage_Error => raise espacio_desbordado;
   end poner;
   
   ----------------------------------------------------------------------------
   -- BORRAR_PRIMERO(): Borra el primer elemento de la cola.
   ----------------------------------------------------------------------------
   procedure borrar_primero(qu: in out cola) is
      q: pnodo renames qu.q;
      p: pnodo renames qu.p;
   begin
      q:= q.sig;
      if q=null then 
         p:=null;
      end if;
   exception
      when Constraint_Error => raise mal_uso;
   end borrar_primero;
   
   ----------------------------------------------------------------------------
   -- COGER_PRIMERO(): Devuelve el elemento primero de la cola.
   ----------------------------------------------------------------------------
   function coger_primero(qu: in cola) return elem is 
      q: pnodo renames qu.q;
   begin 
      return q.x;
   exception
      when Constraint_Error => raise mal_uso;
   end coger_primero;
   
   ----------------------------------------------------------------------------
   -- ESTA_VACIA(): Comprueba si la cola esta o no vacía y devuelve un booleano.
   ----------------------------------------------------------------------------   
   function esta_vacia(qu: in cola) return boolean is
      q: pnodo renames qu.q;
   begin
      return q=null;
   end esta_vacia;
end dcola;
