with Ada.Text_IO;
use Ada.Text_IO;

package body dtauler is

   procedure empty (t: out tauler) is
   begin
      t := (others => (others => peces'First));
   end empty;

   procedure print (t: in tauler) is
      p: peces;
      s: String(1..3);
   begin
      New_Line;
      for i in t'Range(1) loop
         for j in t'Range(2) loop
            p := t(i, j);
            s := p'Image;
            put(s(2) & " ");
         end loop;
         new_line;
      end loop;
      New_Line;
   end print;

   procedure mouJugador(t: in out tauler; numJugador: in integer; cella: in tcella) is
   begin
      t(cella.fila, cella.columna) := peces'Val(numJugador);
   end mouJugador;

   function getDimensio(t: in tauler) return integer is
   begin
      return dimensio;
   end getDimensio;

   function getNumJugadors(t: in tauler) return integer is
   begin
      return numJugadors;
   end getNumJugadors;

   function isCasellaBuida(t: in tauler; cella: in tcella) return Boolean is
   begin
      return t(cella.fila, cella.columna) = peces'First;
   end isCasellaBuida;

   procedure clone (t1: in tauler; t2: out tauler) is
   begin
      t2 := t1;
   end clone;

   function getJugador(p: in string) return integer is
   begin
      return peces'Pos(peces'Value("'"& p &"'"));
   end getJugador;

   function isTaulerComplet(t: in tauler) return Boolean is
   begin
      -- Recorremos filas
      for fila in 1..dimensio loop
         -- Recorremos columnas
         for col in 1..dimensio loop
            -- Si hay alguna celda que tenga '-' es que el tablero
            -- no esta completo
            if t(fila, col) = peces'First then
               -- Devolvemos falso
               return False;
            end if;
         end loop;
      end loop;
      -- Si hemos llegado hasta aqui significa que el tablero esta lleno.
      return true;
   end isTaulerComplet;

   -- Funci� que retorna si el tauler cont�
   -- una disposici� de peces pel jugador 'jugador'
   -- que formin una l�nia (horitzontal o vertical)
   function isLinia (t: in tauler; jugador: in integer) return Boolean is
      piezas : Integer;
      piezaJugador : peces;
   begin
      piezaJugador := peces'Val(jugador);
      piezas := 0;
      for fila in 1..dimensio loop
         for col in 1..dimensio loop
            if t(fila, col) = piezaJugador then
               piezas := piezas + 1;
            end if;
         end loop;
         if piezas = 3 then
            return True;
         end if;
         piezas := 0;
      end loop;

      for col in 1..dimensio loop
         for fila in 1..dimensio loop
            if t(fila, col) = piezaJugador then
               piezas := piezas + 1;
            end if;
         end loop;
         if piezas = 3 then
            return True;
         end if;
         piezas := 0;
      end loop;
      return False;
   end isLinia;

   -- Funci� que retorna si el tauler cont�
   -- una disposici� de peces pel jugador 'jugado'
   -- que formin una diagonal (normal o inversa)
   function isDiagonal (t: in tauler; jugador: in integer) return Boolean is
      piezas : Integer;
      piezaJugador: peces;
   begin
      piezaJugador := peces'Val(jugador);
      piezas := 0;
      for i in 1..dimensio loop
         if t(i, i) = piezaJugador then
               piezas := piezas + 1;
         end if;
         if piezas = 3 then
            return True;
         end if;
      end loop;
      piezas := 0;
      if t(3, 1) = piezaJugador then
         piezas := piezas + 1;
      end if;
      if t(2, 2) = piezaJugador then
         piezas := piezas + 1;
      end if;
      if t(1, 3) = piezaJugador then
         piezas := piezas + 1;
      end if;
      if piezas = 3 then
            return True;
      end if;
      return False;
   end isDiagonal;

   function isJocGuanyat (t: in tauler; jugador: in integer) return boolean is
      -- Funci� per completar
   begin
      if isLinia(t,jugador) = True then
         return True;
      end if;
      if isDiagonal(t,jugador) then
         return True;
      end if;
      return False;
   end isJocGuanyat;

end dtauler;
